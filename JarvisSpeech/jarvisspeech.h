#ifndef JARVISSPEECH_H
#define JARVISSPEECH_H

#include "jarvisinterface.h"
#include "speech_widget.h"
class JarvisSpeech : public JarvisInterface
{
    Q_OBJECT
#ifdef Q_OS_ANDROID

#else
    Q_PLUGIN_METADATA(IID "com.finch.JarvisSpeech" FILE "test.json")
    Q_INTERFACES(JarvisInterface)
#endif

public:
    JarvisSpeech();
    void Init(QString resPath);
    QWidget * GetWidget(QWidget * parent);
    void PollMessage(Type from,QJsonObject data);
    ~JarvisSpeech();
private:
    SpeechWidget * tab;
    QString resPath;
public slots:
    void ChangeState(bool state);
    void ReceiveReply(QJsonObject data);
    void ReceiveSTT(QString data);
    void ButtonState(bool state);
};

#endif // JARVISSPEECH_H
