TEMPLATE = subdirs

SUBDIRS += \
    Saba \
    Bullet \
#    TestOffScreenRender \
    JarvisInterface \
#    TestInterface \
    JarvisAgent \
    JarvisSpeech \
    JarvisWake \
    JarvisAPP \
    JarvisHome \
    JarvisEasyController \
    JarvisSchool \
    JarvisMediaController \
    Jarvis

DISTFILES += README.md LICENSE .gitignore TODO.md
