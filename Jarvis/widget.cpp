#include "widget.h"
#include "ui_widget.h"

#ifdef Q_OS_ANDROID
#include "jarvisspeech.h"
#include "jarvisagent.h"
#include "jarvisapp.h"
#include "jarviseasycontroller.h"
#include "jarvisschool.h"
#endif

#include <QMetaEnum>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
#ifdef Q_OS_ANDROID
        ui->lineEdit->setText("assets:/resource/");
        InitPlugin(new JarvisSpeech,"语音设置");
        InitPlugin(new JarvisAgent,"主界面");
        InitPlugin(new JarvisAPP,"APP");
        InitPlugin(new JarvisEasyController,"EasyController");
        InitPlugin(new JarvisSchool,"JarvisSchool");
        ui->tabWidget->removeTab(0);
        ui->tabWidget->setCurrentIndex(1);
#else
        ui->lineEdit->setText("../resource/");
        emit on_pushButton_clicked();
        emit on_pushButton_2_clicked();
#endif
}

Widget::~Widget()
{
    ReleaseAllInterface();
    delete ui;
}

//Refresh Plugin Lists
void Widget::on_pushButton_clicked()
{
    ui->tableWidget->clearContents();
    ReleaseAllInterface();

    resDir = QDir(ui->lineEdit->text());
    resDir.cd("plugins");
#ifdef Q_OS_ANDROID
#else
        QCoreApplication::addLibraryPath(ui->lineEdit->text()+"/plugins");
#endif
    pluginList = resDir.entryList(QStringList()<<"*.so"<<"*.dll",QDir::Files);

    ui->tableWidget->setRowCount(pluginList.count());
    for(int i=0;i<pluginList.count();++i){
        ui->tableWidget->setItem(i,0,new QTableWidgetItem(pluginList[i]));
    }
}

//Load All Plugins
void Widget::on_pushButton_2_clicked()
{
    ReleaseAllInterface();
    foreach(QString filename,pluginList){
        QPluginLoader plus(filename);
        QObject *obj = plus.instance();
        if(obj){
            qDebug()<< plus.metaData();
            JarvisInterface * mylib = qobject_cast<JarvisInterface *> (obj);
            connect(mylib,&JarvisInterface::SendData,this,&Widget::SendData);
            mylib->Init(ui->lineEdit->text());
            interfaces.push_back(mylib);
            QWidget * widget = mylib->GetWidget(this);
            if(widget != nullptr){
                ui->tabWidget->addTab(widget,filename);
            }
        }else{
            qDebug()<<filename<<"Load Error";
        }
    }
}

QString EnumToString(JarvisInterface::Type mtype){
    return QMetaEnum::fromType<JarvisInterface::Type> ().valueToKey(mtype);
}

void Widget::SendData(JarvisInterface::Type to, QJsonObject data)
{
    JarvisInterface * mylib = qobject_cast<JarvisInterface *> (QObject::sender());
    ui->plainTextEdit_2->insertPlainText("[From]"+EnumToString(mylib->m_type)+"\n");
    ui->plainTextEdit_2->insertPlainText("[To]"+EnumToString(to)+"\n");
    ui->plainTextEdit_2->insertPlainText(QJsonDocument(data).toJson()+"\n");
    foreach(JarvisInterface *item,interfaces){
        if(to == JarvisInterface::Type::ALL || item->m_type == JarvisInterface::Type::ALL || to == item->m_type){
            if(mylib == nullptr)qDebug()<<"[Error]The library can't been load";
            else item->PollMessage(mylib->m_type,data);
        }
    }
}

void Widget::ReleaseAllInterface()
{
    for(int i=0;i<ui->tabWidget->count()-1;++i){
        ui->tabWidget->removeTab(1);
    }
    foreach(JarvisInterface *item,interfaces){
        delete item;
    }
    interfaces.clear();
}

void Widget::InitPlugin(JarvisInterface *mylib,QString tabName)
{
    connect(mylib,&JarvisInterface::SendData,this,&Widget::SendData);
    mylib->Init(ui->lineEdit->text());
    interfaces.push_back(mylib);
    QWidget * widget = mylib->GetWidget(this);
    if(widget != nullptr){
        ui->tabWidget->addTab(widget,tabName);
    }
}

void Widget::on_pushButton_3_clicked()
{
    foreach(JarvisInterface *item,interfaces){
        ui->plainTextEdit->toPlainText();
        item->PollMessage(JarvisInterface::Type::ALL,QJsonDocument::fromJson(ui->plainTextEdit->toPlainText().toLatin1()).object());
    }
}
