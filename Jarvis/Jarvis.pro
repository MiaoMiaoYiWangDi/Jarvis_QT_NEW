#-------------------------------------------------
#
# Project created by QtCreator 2019-07-27T16:48:54
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Jarvis
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        main.cpp \
        widget.cpp

HEADERS += \
        widget.h

FORMS += \
        widget.ui

CONFIG += mobility
MOBILITY = 

#DESTDIR         = ../

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../JarvisInterface/release/ -lJarvisInterface
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../JarvisInterface/debug/ -lJarvisInterface
else:unix: LIBS += -L$$OUT_PWD/../JarvisInterface/ -lJarvisInterface

INCLUDEPATH += $$PWD/../JarvisInterface
DEPENDPATH += $$PWD/../JarvisInterface

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../JarvisInterface/release/libJarvisInterface.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../JarvisInterface/debug/libJarvisInterface.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../JarvisInterface/release/JarvisInterface.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../JarvisInterface/debug/JarvisInterface.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../JarvisInterface/libJarvisInterface.a

DISTFILES += \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat \
    android/src/com/finch/jarvis/*.java

if(contains(ANDROID_TARGET_ARCH,armeabi-v7a) || contains(ANDROID_TARGET_ARCH,x86) || contains(ANDROID_TARGET_ARCH,x86_64) || contains(ANDROID_TARGET_ARCH,arm64-v8a)) {
    win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../JarvisSpeech/release/ -lJarvisSpeech
    else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../JarvisSpeech/debug/ -lJarvisSpeech
    else:unix: LIBS += -L$$OUT_PWD/../JarvisSpeech/ -lJarvisSpeech

    INCLUDEPATH += $$PWD/../JarvisSpeech
    DEPENDPATH += $$PWD/../JarvisSpeech

    win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../JarvisSpeech/release/libJarvisSpeech.a
    else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../JarvisSpeech/debug/libJarvisSpeech.a
    else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../JarvisSpeech/release/JarvisSpeech.lib
    else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../JarvisSpeech/debug/JarvisSpeech.lib
    else:unix: PRE_TARGETDEPS += $$OUT_PWD/../JarvisSpeech/libJarvisSpeech.a
    #-------------------------add static plugin lib ----------------------------
    win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../JarvisAgent/release/ -lJarvisAgent
    else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../JarvisAgent/debug/ -lJarvisAgent
    else:unix: LIBS += -L$$OUT_PWD/../JarvisAgent/ -lJarvisAgent

    INCLUDEPATH += $$PWD/../JarvisAgent
    DEPENDPATH += $$PWD/../JarvisAgent

    win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../JarvisAgent/release/libJarvisAgent.a
    else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../JarvisAgent/debug/libJarvisAgent.a
    else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../JarvisAgent/release/JarvisAgent.lib
    else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../JarvisAgent/debug/JarvisAgent.lib
    else:unix: PRE_TARGETDEPS += $$OUT_PWD/../JarvisAgent/libJarvisAgent.a
    #-------------------------add static plugin lib ----------------------------
    win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../JarvisAPP/release/ -lJarvisAPP
    else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../JarvisAPP/debug/ -lJarvisAPP
    else:unix: LIBS += -L$$OUT_PWD/../JarvisAPP/ -lJarvisAPP

    INCLUDEPATH += $$PWD/../JarvisAPP
    DEPENDPATH += $$PWD/../JarvisAPP

    win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../JarvisAPP/release/libJarvisAPP.a
    else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../JarvisAPP/debug/libJarvisAPP.a
    else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../JarvisAPP/release/JarvisAPP.lib
    else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../JarvisAPP/debug/JarvisAPP.lib
    else:unix: PRE_TARGETDEPS += $$OUT_PWD/../JarvisAPP/libJarvisAPP.a
    #-------------------------add static plugin lib ----------------------------

    win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../JarvisEasyController/release/ -lJarvisEasyController
    else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../JarvisEasyController/debug/ -lJarvisEasyController
    else:unix: LIBS += -L$$OUT_PWD/../JarvisEasyController/ -lJarvisEasyController

    INCLUDEPATH += $$PWD/../JarvisEasyController
    DEPENDPATH += $$PWD/../JarvisEasyController

    win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../JarvisEasyController/release/libJarvisEasyController.a
    else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../JarvisEasyController/debug/libJarvisEasyController.a
    else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../JarvisEasyController/release/JarvisEasyController.lib
    else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../JarvisEasyController/debug/JarvisEasyController.lib
    else:unix: PRE_TARGETDEPS += $$OUT_PWD/../JarvisEasyController/libJarvisEasyController.a
    #-------------------------add static plugin lib ----------------------------

    win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../JarvisSchool/release/ -lJarvisSchool
    else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../JarvisSchool/debug/ -lJarvisSchool
    else:unix: LIBS += -L$$OUT_PWD/../JarvisSchool/ -lJarvisSchool

    INCLUDEPATH += $$PWD/../JarvisSchool
    DEPENDPATH += $$PWD/../JarvisSchool

    win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../JarvisSchool/release/libJarvisSchool.a
    else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../JarvisSchool/debug/libJarvisSchool.a
    else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../JarvisSchool/release/JarvisSchool.lib
    else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../JarvisSchool/debug/JarvisSchool.lib
    else:unix: PRE_TARGETDEPS += $$OUT_PWD/../JarvisSchool/libJarvisSchool.a
    #-------------------------add static plugin lib ----------------------------

    INCLUDEPATH += \
        ../External/glm/include \
        ../External/spdlog/include \
        ../External/tinyddsloader/include \
        ../External/tinyxfileloader/include \
        ../External/tinyobjloader/include \
        ../Bullet/src

    win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../Saba/release/ -lSaba
    else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../Saba/debug/ -lSaba
    else:unix: LIBS += -L$$OUT_PWD/../Saba/ -lSaba

    INCLUDEPATH += $$PWD/../Saba
    DEPENDPATH += $$PWD/../Saba

    win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../Saba/release/libSaba.a
    else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../Saba/debug/libSaba.a
    else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../Saba/release/Saba.lib
    else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../Saba/debug/Saba.lib
    else:unix: PRE_TARGETDEPS += $$OUT_PWD/../Saba/libSaba.a

    #-------------------------add saba lib ----------------------------

    win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../Bullet/release/ -lBullet
    else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../Bullet/debug/ -lBullet
    else:unix: LIBS += -L$$OUT_PWD/../Bullet/ -lBullet

    INCLUDEPATH += $$PWD/../Bullet
    DEPENDPATH += $$PWD/../Bullet

    win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../Bullet/release/libBullet.a
    else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../Bullet/debug/libBullet.a
    else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../Bullet/release/Bullet.lib
    else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../Bullet/debug/Bullet.lib
    else:unix: PRE_TARGETDEPS += $$OUT_PWD/../Bullet/libBullet.a

    #-------------------------add bullet lib ----------------------------
    CONFIG += c++14 gnu++14

    QT += network multimedia texttospeech androidextras
}else{
    CONFIG += c++11
}

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

CONFIG += opensslv11

include(../External/android_openssl/openssl.pri)
