package com.finch.jarvis;

import android.content.Context;
import android.graphics.PixelFormat;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

public class TestAndroidAPI {
    Context context;
    WindowManager wm;
    WindowManager.LayoutParams params;
    float fx,fy;
    public static boolean isadd = false;
    Button button;
    boolean bDownflag = false;

    public native void CallBack();

    public TestAndroidAPI(Context context){
        this.context = context;
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                setwmparams(0,0);
                ShowToast();
            }
        });
    }
    private void setwmparams(int x,int y){
        wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        params = new WindowManager.LayoutParams();
        params.type = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        params.format = PixelFormat.RGBA_8888;
        params.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;//|WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;
        params.width=128;
        params.height=128;
        params.x = x;
        params.y = y;
    }
    public void ShowToast(){
        if(!isadd){//防止重复添加
            //flowWindow = View.inflate(context,R.layout.activity_main,null);
            button = new Button(context);
            button.setBackgroundResource(android.R.drawable.ic_btn_speak_now);
            button.setText("");
            wm.addView(button,params);
            isadd=true;
        }
        button.setOnTouchListener(new View.OnTouchListener() {
            int lastX, lastY;
            int paramX, paramY;
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        lastX = (int) event.getRawX();
                        lastY = (int) event.getRawY();
                        paramX = params.x;
                        paramY = params.y;
                        bDownflag = true;
                        break;
                    case MotionEvent.ACTION_MOVE:
                        int dx = (int) event.getRawX() - lastX;
                        int dy = (int) event.getRawY() - lastY;
                        params.x = paramX + dx;
                        params.y = paramY + dy;
                        fx=params.x;fy=params.y;
                        //更新悬浮窗位置
                        wm.updateViewLayout(button, params);
                        bDownflag = false;
                        break;
                    case MotionEvent.ACTION_UP:
                        if(bDownflag){
                            CallBack();
                        }
                        break;
                }
                return true;
            }
        });
    }
}
