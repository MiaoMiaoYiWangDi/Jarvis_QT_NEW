#ifndef JARVISSCHOOL_H
#define JARVISSCHOOL_H

#include "jarvisinterface.h"
#include <QJsonArray>
#include <QList>
#include <QMap>
#include <QFile>
#include <QJsonDocument>
#include <QTime>
#include <QDateTime>

class JarvisSchool : public JarvisInterface
{
    Q_OBJECT
#ifdef Q_OS_ANDROID

#else
    Q_PLUGIN_METADATA(IID "com.finch.JarvisSchool" FILE "test.json")
    Q_INTERFACES(JarvisInterface)
#endif

public:
    JarvisSchool();
    void Init(QString resPath);
    QWidget * GetWidget(QWidget * parent);
    void PollMessage(Type from,QJsonObject data);
    ~JarvisSchool();
private:
    QJsonArray myclass;
    int TimeToIndex();
};

#endif // JARVISSCHOOL_H
