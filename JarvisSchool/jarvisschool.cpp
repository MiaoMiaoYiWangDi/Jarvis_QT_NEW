#include "jarvisschool.h"

JarvisSchool::JarvisSchool()
{
    m_type = Type::OTHER;
}

void JarvisSchool::Init(QString resPath)
{
    QFile file(resPath+"data/school.json");
    file.open(QIODevice::ReadOnly);
    myclass = QJsonDocument::fromJson(file.readAll()).array();
}

QWidget *JarvisSchool::GetWidget(QWidget *parent)
{
    return nullptr;
}

void JarvisSchool::PollMessage(JarvisInterface::Type from, QJsonObject data)
{
    if(from == Type::SPEECH){
        if(data["MODE"] == "AIUI"){
            data = data["data"].toObject()["data"].toArray()[0].toObject();
            QString service = data["intent"].toObject()["service"].toString();
            if(service == "AIUI.chInterest"){
                service = data["intent"].toObject()["semantic"].toArray()[0].toObject()["slots"].toArray()[0].toObject()["value"].toString();
                if(service == "课程表"){
                    QDate cur_date = QDate::currentDate();
                    int week = cur_date.dayOfWeek() - 1;
                    QString ret;
                    if(week > 4)ret = "您目前没课";
                    else{
                        int index = TimeToIndex();
                        QJsonObject msg = myclass[week].toArray()[index].toObject();
                        if(msg["name"].toString()!="" && index > 0) ret = "您即将有一节"+msg["name"].toString()+"，地点是"+msg["pos"].toString();
                        else ret = "您目前没课";
                    }
                    QJsonObject obj;
                    obj["MODE"] = "Text";
                    obj["text"] = ret;
                    emit SendData(Type::SPEECH,obj);
                }
            }
        }
    }
}

JarvisSchool::~JarvisSchool()
{

}

int JarvisSchool::TimeToIndex()
{
    QTime time = QTime::currentTime();
    QTime times[4] = {QTime(8,0),QTime(10,0),QTime(14,0),QTime(16,0)};
    for(int i=0;i<4;++i)if(time<times[i])return i;
    return -1;
}
