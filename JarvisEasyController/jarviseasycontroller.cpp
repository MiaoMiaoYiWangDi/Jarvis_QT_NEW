#include "jarviseasycontroller.h"

JarvisEasyController * JarvisEasyController::m_instance = nullptr;

JarvisEasyController::JarvisEasyController()
{
    m_type = Type::OTHER;
    this->m_instance = this;
}

void JarvisEasyController::Init(QString resPath)
{
    ShowController();
}

QWidget *JarvisEasyController::GetWidget(QWidget *parent)
{
    return nullptr;
}

void JarvisEasyController::PollMessage(JarvisInterface::Type from, QJsonObject data)
{

}

JarvisEasyController::~JarvisEasyController()
{

}
#ifdef Q_OS_ANDROID
void JarvisEasyController::ShowController()
{
    QAndroidJniEnvironment env;
    auto MyClass = env->FindClass("com/finch/jarvis/TestAndroidAPI");
    static const JNINativeMethod gMethods[] = {{"CallBack","()V",(void*)CallBack}};
    env->RegisterNatives(MyClass,gMethods,1);
    QAndroidJniObject AndroidAPI("com/finch/jarvis/TestAndroidAPI","(Landroid/content/Context;)V",QtAndroid::androidContext().object());
}
#else
void JarvisEasyController::ShowController()
{

}
#endif

#ifdef Q_OS_ANDROID
void JarvisEasyController::CallBack()
{
    QJsonObject obj;
    obj["MODE"] = "Click";
    qRegisterMetaType<Type>("Type");
    emit m_instance->SendData(Type::SPEECH,obj);
}
#else

#endif
