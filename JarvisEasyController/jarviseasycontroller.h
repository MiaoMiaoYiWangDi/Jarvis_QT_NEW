#ifndef JARVISEASYCONTROLLER_H
#define JARVISEASYCONTROLLER_H

#include "jarvisinterface.h"
#include <QJsonArray>
#include <QDebug>
#ifdef Q_OS_ANDROID
    #include <QtAndroid>
    #include <QAndroidJniObject>
    #include <QAndroidIntent>
    #include <QAndroidJniEnvironment>
#endif

class  JarvisEasyController : public JarvisInterface
{
    Q_OBJECT
#ifdef Q_OS_ANDROID

#else
    Q_PLUGIN_METADATA(IID "com.finch.JarvisEasyController" FILE "test.json")
    Q_INTERFACES(JarvisInterface)
#endif

public:
    JarvisEasyController();
    void Init(QString resPath);
    QWidget * GetWidget(QWidget * parent);
    void PollMessage(Type from,QJsonObject data);
    ~JarvisEasyController();
private:
#ifdef Q_OS_ANDROID

#endif
    void ShowController();
    static void CallBack();
    static JarvisEasyController *m_instance;
};

#endif // JARVISAPP_H
