#ifndef JarvisWake_H
#define JarvisWake_H

#include "jarvisinterface.h"
#include <QJsonArray>
#include <QJsonDocument>
#include <QAudioDeviceInfo>
#include <QAudioFormat>
#include <QAudioInput>
#include <QDebug>
#include <pv_porcupine.h>

class JarvisWake : public JarvisInterface
{
    Q_OBJECT
#ifdef Q_OS_ANDROID

#else
    Q_PLUGIN_METADATA(IID "com.finch.JarvisWake" FILE "test.json")
    Q_INTERFACES(JarvisInterface)
#endif

public:
    JarvisWake();
    void Init(QString resPath);
    QWidget * GetWidget(QWidget * parent);
    void PollMessage(Type from,QJsonObject data);
    ~JarvisWake();
private:
    const QString modelPath = "porcupine_params.pv";
    const QString keywordPath = "jarvis_linux.ppn";
    pv_porcupine_object_t *porcupineObject;
    QAudioInput *audioInput;
    QIODevice *ioDevice;
    QByteArray audioDataBuffer;
public:
    void listen(const QByteArray &audioData);
    void processSamples(int porcupineFrameLength);
    bool hasEnoughSamples(int porcupineFrameLength);
};

#endif // JarvisWake_H
