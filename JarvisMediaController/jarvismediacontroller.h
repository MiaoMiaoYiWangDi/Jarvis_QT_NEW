#ifndef JarvisMediaController_H
#define JarvisMediaController_H

#include "jarvisinterface.h"
#include <QJsonArray>
#include <QList>
#include <QJsonDocument>
#ifdef Q_OS_ANDROID

#else
    #include <QtDBus>
#endif

class JarvisMediaController : public JarvisInterface
{
    Q_OBJECT
#ifdef Q_OS_ANDROID

#else
    Q_PLUGIN_METADATA(IID "com.finch.JarvisMediaController" FILE "test.json")
    Q_INTERFACES(JarvisInterface)
#endif

public:
    JarvisMediaController();
    void Init(QString resPath);
    QWidget * GetWidget(QWidget * parent);
    void PollMessage(Type from,QJsonObject data);
    ~JarvisMediaController();
private:
    QStringList services;
    const QString playerPrefix = "org.mpris.MediaPlayer2.";
#ifdef Q_OS_ANDROID
#else
    QDBusInterface *inter;
#endif

};

#endif // JarvisMediaController_H
