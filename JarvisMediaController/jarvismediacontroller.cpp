#include "jarvismediacontroller.h"

JarvisMediaController::JarvisMediaController()
{
    m_type = Type::OTHER;
}

void JarvisMediaController::Init(QString resPath)
{
#ifdef Q_OS_ANDROID
#else
    QDBusConnectionInterface* bus = QDBusConnection::sessionBus().interface();
    QDBusReply<QStringList> reply = bus->registeredServiceNames();
    if (reply.isValid()) {
        QStringList m_services = reply.value();
        foreach (const QString& name, m_services)
            if (name.startsWith(playerPrefix))
                services<<name;
    }
    qDebug()<<services;
    if(services.length()>0) inter = new QDBusInterface(services[0],"/org/mpris/MediaPlayer2","org.mpris.MediaPlayer2.Player",QDBusConnection::sessionBus(), this);
#endif
}

QWidget *JarvisMediaController::GetWidget(QWidget *parent)
{
    return nullptr;
}

void JarvisMediaController::PollMessage(JarvisInterface::Type from, QJsonObject data)
{
    if(from == Type::SPEECH){
        if(data["MODE"] == "AIUI"){
            data = data["data"].toObject()["data"].toArray()[0].toObject()["intent"].toObject();
            if(data["service"].toString() == "musicX" || data["service"].toString() ==  "musicPlayer_smartHome"){
#ifdef Q_OS_ANDROID
#else
                inter->call("PlayPause");
                qDebug()<<"call media controller!";
#endif
            }
        }
    }
}

JarvisMediaController::~JarvisMediaController()
{

}
