QT += core gui widgets dbus

TEMPLATE = lib

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    jarvismediacontroller.cpp

HEADERS += \
    jarvismediacontroller.h

# Default rules for deployment.
unix {
    target.path = $$[QT_INSTALL_PLUGINS]/generic
}
!isEmpty(target.path): INSTALLS += target


win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../JarvisInterface/release/ -lJarvisInterface
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../JarvisInterface/debug/ -lJarvisInterface
else:unix: LIBS += -L$$OUT_PWD/../JarvisInterface/ -lJarvisInterface

INCLUDEPATH += $$PWD/../JarvisInterface
DEPENDPATH += $$PWD/../JarvisInterface

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../JarvisInterface/release/libJarvisInterface.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../JarvisInterface/debug/libJarvisInterface.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../JarvisInterface/release/JarvisInterface.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../JarvisInterface/debug/JarvisInterface.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../JarvisInterface/libJarvisInterface.a

if(contains(ANDROID_TARGET_ARCH,armeabi-v7a) || contains(ANDROID_TARGET_ARCH,x86) || contains(ANDROID_TARGET_ARCH,x86_64) || contains(ANDROID_TARGET_ARCH,arm64-v8a)) {
    CONFIG += staticlib
    QT += androidextras
}else{
    DESTDIR = ../resource/plugins

}
