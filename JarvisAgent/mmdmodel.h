#ifndef MMDMODEL_H
#define MMDMODEL_H

#include <Saba/Base/Time.h>
#include <Saba/Model/MMD/PMDModel.h>
#include <Saba/Model/MMD/PMXModel.h>
#include <Saba/Model/MMD/VMDFile.h>
#include <Saba/Model/MMD/VMDAnimation.h>
#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLFunctions>
#include "mmdscene.h"
#include "mmdshader.h"
#include "mmdmaterial.h"

#include <QDebug>

class Model : protected QOpenGLFunctions{
public:
    explicit Model(QString modelFile,QString MMDPath,MMDScene * scene);
    ~Model();
    void Draw();
    void Update();
    void InitMorph();
    void AutoMonth();
    void AutoBlink();
private:
    QOpenGLBuffer m_posVBO;
    QOpenGLBuffer m_norVBO;
    QOpenGLBuffer m_uvVBO;
    QOpenGLBuffer m_ibo;
    QOpenGLVertexArrayObject m_mmdVAO;
    GLenum m_indexType;
private:
    QList<Material> m_materials;
    MMDScene * scene;
private:
    GLuint	m_dummyColorTex = 0;
    GLuint	m_dummyShadowDepthTex = 0;
public:
    bool month_enable = false;
    float month_speed = 12.0f;
    float Blink_speed = 12.0f;
    bool Blink_enable = false;
    float Blink_last = 0.2f;
    float Blink_time = 0.0f;
    float Blink_interval = 2.0f;
    saba::MMDMorph *eye,*month;
    bool Anim_Loop = false;
    saba::VMDAnimation * anim;
    std::shared_ptr<saba::PMXModel> model;
    double saveTime = saba::GetTime();
    double passtime = 0;
    double elapsed;
    int frames;
};

#endif // MMDMODEL_H
