#include "mmdscene.h"

QOpenGLTexture *MMDScene::GetTexture(QString filepath)
{
    if(textures.find(filepath) != textures.end())return textures[filepath];
    QOpenGLTexture * texture = new QOpenGLTexture(QImage(filepath).mirrored());
    if(texture->textureId() == 0)qDebug()<<filepath;
    texture->setMinificationFilter(QOpenGLTexture::Linear);
    texture->setMagnificationFilter(QOpenGLTexture::Linear);
    texture->setWrapMode(QOpenGLTexture::Repeat);
    textures[filepath] = texture;
    return texture;
}

MMDScene::MMDScene(QString vertexFile, QString fragFile)
{
    shader = new MMDShader(vertexFile,fragFile);
}

MMDScene::~MMDScene()
{
    foreach(QOpenGLTexture * item,textures){
        //delete item;
        Q_UNUSED(item)
        //[WARN]the item may be deleted while the context is deleted
    }
    delete shader;
}
