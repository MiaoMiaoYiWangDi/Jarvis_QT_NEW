#ifndef JARVISAGENT_H
#define JARVISAGENT_H
#include "jarvisinterface.h"
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include "agentglwidget.h"
class JarvisAgent : public JarvisInterface
{
    Q_OBJECT
#ifdef Q_OS_ANDROID

#else
    Q_PLUGIN_METADATA(IID "com.finch.JarvisAgent" FILE "test.json")
    Q_INTERFACES(JarvisInterface)
#endif

public:
    JarvisAgent();
    void Init(QString resPath);
    QWidget * GetWidget(QWidget * parent);
    void PollMessage(Type from,QJsonObject data);
    ~JarvisAgent();
private slots:
    void ButtonClick();
private:
    QString resPath;
    QWidget *tab;
    QPushButton * button;
    QLabel * label;
    AgentGLWidget *agent;
    bool buttonState = true;
};

#endif // JARVISAGENT_H
