#include "testinterface.h"

TestInterface::TestInterface()
{
    m_type = Type::OTHER;
    qDebug()<<"I start";
    //warning: you can't send message when initializing.because no one receive your message.
}

void TestInterface::Init(QString resPath)
{
    qDebug()<<resPath;
}

QWidget *TestInterface::GetWidget(QWidget *parent)
{
    tab = new QWidget();
    tab->setObjectName(QStringLiteral("testTab"));
    verticalLayout_2 = new QVBoxLayout(tab);
    verticalLayout_2->setSpacing(6);
    verticalLayout_2->setContentsMargins(11, 11, 11, 11);
    verticalLayout_2->setObjectName(QStringLiteral("layout0"));
    pushButton = new QPushButton(tab);
    pushButton->setText("你好，我是测试");
    pushButton->setObjectName(QStringLiteral("push1"));

    verticalLayout_2->addWidget(pushButton);

    pushButton_2 = new QPushButton(tab);
    pushButton_2->setText("你好，我是测试2222");
    pushButton_2->setObjectName(QStringLiteral("push2"));

    verticalLayout_2->addWidget(pushButton_2);

    return tab;
}

void TestInterface::PollMessage(JarvisInterface::Type from, QJsonObject data)
{
    qDebug()<<from<<data;
    QJsonObject obj;
    obj["hello"]="fuck you";
    emit SendData(Type::AGENT,obj);
}

TestInterface::~TestInterface()
{
    //warning : you must delete your object here.because it may cause out of memory.
    if(tab != nullptr) delete tab;
    qDebug()<<"I have been released";
}
