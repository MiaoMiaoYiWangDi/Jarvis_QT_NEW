#include "widget.h"
#include "ui_widget.h"

void Widget::mousePressEvent(QMouseEvent *event)
{
    if(!enableMouse)return;
    if(event->button() == Qt::LeftButton){
        oldPos = event->globalPos() - frameGeometry().topLeft();
    }else if(event->button() == Qt::RightButton){
        if(QAction *act = menu.exec(QCursor::pos())){
            if(act->text()=="退出"){close();exit(0);}
        }
    }
}

void Widget::mouseMoveEvent(QMouseEvent *event)
{
    if(!enableMouse)return;
    if(event->buttons() & Qt::LeftButton){
        move(event->globalPos() - oldPos);
    }
}

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget),
    menu("RightClickMenu")
{
#ifdef Q_OS_ANDROID
        enableMouse = false;
#else
        enableMouse = true;
        setAttribute(Qt::WA_TranslucentBackground, true);
        //setAttribute(Qt::WA_TransparentForMouseEvents,true);
        setWindowFlags(windowFlags() | Qt::WindowStaysOnTopHint);
#endif
    if(enableMouse)setWindowFlags(windowFlags() | Qt::FramelessWindowHint);
    ui->setupUi(this);
    QSurfaceFormat format;
    format.setSamples(16);
    format.setSwapInterval(1);
    ui->openGLWidget->setFormat(format);

    menu.addAction("退出");
    //menu.addAction("two");
}

Widget::~Widget()
{
    delete ui;
}
