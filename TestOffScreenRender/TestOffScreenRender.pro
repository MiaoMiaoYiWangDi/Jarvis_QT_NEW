#-------------------------------------------------
#
# Project created by QtCreator 2019-07-24T20:56:02
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TestOffScreenRender
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        main.cpp \
        widget.cpp

HEADERS += \
        widget.h \
    myglwidget.h

FORMS += \
        widget.ui

CONFIG += c++14 gnu++14

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../Saba/release/ -lSaba
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../Saba/debug/ -lSaba
else:unix: LIBS += -L$$OUT_PWD/../Saba/ -lSaba

INCLUDEPATH += $$PWD/../Saba
DEPENDPATH += $$PWD/../Saba

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../Saba/release/libSaba.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../Saba/debug/libSaba.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../Saba/release/Saba.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../Saba/debug/Saba.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../Saba/libSaba.a

#-------------------------add saba lib ----------------------------

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../Bullet/release/ -lBullet
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../Bullet/debug/ -lBullet
else:unix: LIBS += -L$$OUT_PWD/../Bullet/ -lBullet

INCLUDEPATH += $$PWD/../Bullet
DEPENDPATH += $$PWD/../Bullet

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../Bullet/release/libBullet.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../Bullet/debug/libBullet.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../Bullet/release/Bullet.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../Bullet/debug/Bullet.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../Bullet/libBullet.a

#-------------------------add bullet lib ----------------------------

linux{
    #LIBS += -lpthread
}

INCLUDEPATH += \
    ../External/glm/include \
    ../External/spdlog/include \
    ../External/tinyddsloader/include \
    ../External/tinyxfileloader/include \
    ../External/tinyobjloader/include \
    ../Bullet/src

RESOURCES +=

DISTFILES += \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat

contains(ANDROID_TARGET_ARCH,x86) {
    ANDROID_PACKAGE_SOURCE_DIR = \
        $$PWD/android
}
