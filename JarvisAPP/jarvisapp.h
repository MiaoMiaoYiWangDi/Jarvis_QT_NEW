#ifndef JARVISAPP_H
#define JARVISAPP_H

#include "jarvisinterface.h"
#include <QJsonArray>
#include <QList>
#include <QMap>
#include <QProcess>
#include <QDebug>
#ifdef Q_OS_ANDROID
    #include <QtAndroid>
    #include <QAndroidJniObject>
    #include <QAndroidIntent>
    #include <QAndroidJniEnvironment>
#endif

class  JarvisAPP : public JarvisInterface
{
    Q_OBJECT
#ifdef Q_OS_ANDROID

#else
    Q_PLUGIN_METADATA(IID "com.finch.JarvisAPP" FILE "test.json")
    Q_INTERFACES(JarvisInterface)
#endif

public:
    JarvisAPP();
    void Init(QString resPath);
    QWidget * GetWidget(QWidget * parent);
    void PollMessage(Type from,QJsonObject data);
    ~JarvisAPP();
private:
#ifdef Q_OS_ANDROID
    QAndroidJniObject manager = QtAndroid::androidContext().callObjectMethod("getPackageManager","()Landroid/content/pm/PackageManager;");
#endif
    void openAPP(QString packName);
    QMap<QString,QString> getApplications();
    QMap<QString, QString> apps;
};

#endif // JARVISAPP_H
